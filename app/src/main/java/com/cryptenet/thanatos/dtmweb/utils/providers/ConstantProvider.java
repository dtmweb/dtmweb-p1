/*
 * Copyright (c) 2018.
 * Development Courtesy: Cryptenet Ltd.
 * Developer Credit: Alamgir Hossain, Nabil Shawkat
 * This project is under MIT license
 */

package com.cryptenet.thanatos.dtmweb.utils.providers;

public class ConstantProvider {
    public static final String ACTIVITY_FRAGMENT_MANAGER = "ActivityFragmentManager";

    public static final String NAV_PP_URL = "navPpUrl";
    public static final String NAV_NAME = "navName";
    public static final String NAV_TYPE = "navType";
    public static final String NAV_ADDRESS = "navAddress";
    public static final String NAV_DETAILS = "navDetails";

    public static final String CLIENT_KEY = "xcziSPz2aLcKc5Egm1janzwGBS5jcNvi68BA88GP:VHSlol14f1AKcaXcdefMzsNA9kMKyngVPrSD5FxdxcQzukBzH8axcQRts4NUMdztZ035bSI4RGgzSRUsm4N7h7e106T9SfnijBvSgmRizIyPrXzKetePl5HpO49XQ3LK";
    public static final String ACCESS_BASIC = "eGN6aVNQejJhTGNLYzVFZ20xamFuendHQlM1amNOdmk2OEJBODhHUDpWSFNsb2wxNGYxQUtjYVhjZGVmTXpzTkE5a01LeW5nVlByU0Q1RnhkeGNRenVrQnpIOGF4Y1FSdHM0TlVNZHp0WjAzNWJTSTRSR2d6U1JVc200TjdoN2UxMDZUOVNmbmlqQnZTZ21SaXpJeVByWHpLZXRlUGw1SHBPNDlYUTNMSw==";

    public static final int RESULT_LOAD_IMG = 101;
    public static final int RESULT_FILE_IMG = 102;

    public static final String BASE_URL = "https://fa-sa-801-dev.herokuapp.com/";

    public static final String SP_ACCESS_TOKEN = "ac_token";
    public static final String SP_REFRESH_TOKEN = "rs_token";
    public static final String SP_ID = "u_id";
    public static final String SP_NAME = "u_name";
    public static final String SP_EMAIL = "u_email";
    public static final String SP_ADDRESS = "u_address";
    public static final String SP_CITY = "u_city";
    public static final String SP_COUNTRY = "u_country";
    public static final String SP_PICTURE_URL = "u_picture_url";
    public static final String SP_BANK_NAME = "u_bank_name";
    public static final String SP_BANK_ACC_NAME = "u_bank_acc_name";
    public static final String SP_BANK_ACC_NO = "u_bank_acc_no";
    public static final String SP_USER_TYPE = "u_type";

    public static final String SP_FORGOT_PASSWORD_EMAIL = "forgot_pass";
    public static final String SP_FORGOT_PASSWORD_CODE = "forgot_pass_code";
}
